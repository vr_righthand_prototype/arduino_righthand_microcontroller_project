# define indexMiddlePin 16
# define middleRingPin 17
# define ringPinkyPin 5

# define indexFlexPin 34
# define middleFlexPin 35
# define ringFlexPin 32
# define pinkyFlexPin 33

void setup() {
  Serial.begin(9600);
  
  pinMode(indexMiddlePin, INPUT);
  pinMode(middleRingPin, INPUT);
  pinMode(ringPinkyPin, INPUT);
  
  pinMode(indexFlexPin, INPUT);
  pinMode(middleFlexPin, INPUT);
  pinMode(ringFlexPin, INPUT);
  pinMode(pinkyFlexPin, INPUT);
}

void loop() {
  int IMR = fingerIsOpen(indexMiddlePin);
  int MRR = fingerIsOpen(middleRingPin);
  int RPR = fingerIsOpen(ringPinkyPin);
  
  int IFR = fingerFlexPosition(indexFlexPin);
  int MFR = fingerFlexPosition(middleFlexPin);
  int RFR = fingerFlexPosition(ringFlexPin);
  int PFR = fingerFlexPosition(pinkyFlexPin);
  
  Serial.print("IMR=" + String(IMR) + ',');
  Serial.print("MRR=" + String(MRR) + ',');
  Serial.print("RPR=" + String(RPR) + ',');
  
  Serial.print("IFR=" + String(IFR) + ',');
  Serial.print("MFR=" + String(MFR) + ',');
  Serial.print("RFR=" + String(RFR) + ',');
  Serial.println("PFR=" + String(PFR));
}

int fingerIsOpen(int readPin) {
  int isOpen = digitalRead(readPin); 
  return isOpen;
}

int fingerFlexPosition(int readPin) {
  int isOpen = analogRead(readPin); 
  return isOpen;
}
