#include "I2Cdev.h"
#include "MPU6050.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

MPU6050 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;
#define OUTPUT_READABLE_ACCELGYRO

# define indexMiddlePin 16
# define middleRingPin 17
# define ringPinkyPin 5

# define indexFlexPin 34
# define middleFlexPin 35
# define ringFlexPin 32
# define pinkyFlexPin 33

#define A_R 16384.0 // 32768/2
#define G_R 131.0 // 32768/250
 
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779
 
//Angulos
float Acc[2];
float Gy[3];
float Angle[3];

String valores;

long tiempo_prev;
float dt;
void setup() {
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif
  Serial.begin(9600);

  Serial.println("Initializing I2C devices...");
  accelgyro.initialize();
  Serial.println("Testing device connections...");
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  pinMode(indexMiddlePin, INPUT);
  pinMode(middleRingPin, INPUT);
  pinMode(ringPinkyPin, INPUT);
  
  pinMode(indexFlexPin, INPUT);
  pinMode(middleFlexPin, INPUT);
  pinMode(ringFlexPin, INPUT);
  pinMode(pinkyFlexPin, INPUT);
  
  Serial.print(accelgyro.getXAccelOffset()); Serial.print("\t"); // -76
  Serial.print(accelgyro.getYAccelOffset()); Serial.print("\t"); // -2359
  Serial.print(accelgyro.getZAccelOffset()); Serial.print("\t"); // 1688
  Serial.print(accelgyro.getXGyroOffset()); Serial.print("\t"); // 0
  Serial.print(accelgyro.getYGyroOffset()); Serial.print("\t"); // 0
  Serial.print(accelgyro.getZGyroOffset()); Serial.print("\t"); // 0
  Serial.print("\n");
  
  accelgyro.setXGyroOffset(220);
  accelgyro.setYGyroOffset(76);
  accelgyro.setZGyroOffset(-85);
}

void loop() {
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  int IMR = fingerIsOpen(indexMiddlePin);
  int MRR = fingerIsOpen(middleRingPin);
  int RPR = fingerIsOpen(ringPinkyPin);
  
  int IFR = fingerFlexPosition(indexFlexPin);
  int MFR = fingerFlexPosition(middleFlexPin);
  int RFR = fingerFlexPosition(ringFlexPin);
  int PFR = fingerFlexPosition(pinkyFlexPin);

  #ifdef OUTPUT_READABLE_ACCELGYRO
    Acc[1] = atan(-1*(ax/A_R)/sqrt(pow((ay/A_R),2) + pow((az/A_R),2)))*RAD_TO_DEG;
    Acc[0] = atan((ay/A_R)/sqrt(pow((ax/A_R),2) + pow((az/A_R),2)))*RAD_TO_DEG;
    Gy[0] = gx/G_R;
    Gy[1] = gy/G_R;
    Gy[2] = gz/G_R;
    dt = (millis() - tiempo_prev) / 1000.0;
    tiempo_prev = millis();
    //Aplicar el Filtro Complementario
    Angle[0] = 0.98 *(Angle[0]+Gy[0]*dt) + 0.02*Acc[0];
    Angle[1] = 0.98 *(Angle[1]+Gy[1]*dt) + 0.02*Acc[1];
    //Integración respecto del tiempo paras calcular el YAW
    Angle[2] = Angle[2]+Gy[2]*dt;
    Serial.print("HAX=" + String(round(Angle[0])) + ',');
    Serial.print("HAY=" + String(round(Angle[1])) + ',');
    Serial.print("HAZ=" + String(round(Angle[2])) + ',');
  #endif
  
  Serial.print("IMR=" + String(IMR) + ',');
  Serial.print("MRR=" + String(MRR) + ',');
  Serial.print("RPR=" + String(RPR) + ',');
  
  Serial.print("IFR=" + String(IFR) + ',');
  Serial.print("MFR=" + String(MFR) + ',');
  Serial.print("RFR=" + String(RFR) + ',');
  Serial.println("PFR=" + String(PFR));
}

int fingerIsOpen(int readPin) {
  int isOpen = digitalRead(readPin); 
  return isOpen;
}

int fingerFlexPosition(int readPin) {
  int isOpen = analogRead(readPin); 
  return isOpen;
}
